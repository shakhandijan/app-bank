import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as FaIcons from "react-icons/fa";
import './SearchBar.css'
import axios from 'axios'
import Result from '../Results';

const SearchBar = () => {

    const [searchInput, setSearchInput] = useState('')
    const [repos, setRepos] = useState([])

    const handleChange = (e) => {
        setSearchInput(e.target.value)
    }

    const handleClick = async () => {
        console.log(searchInput)
        try{
            const result = await axios(`https://api.github.com/users/${searchInput}/repos`)

            setRepos(result)
        }catch(e){
            console.log(e);
        }
      
    }
    return (
<>
        <div className="container">
         
                <div class="input-group">
                    <input type="text" class="form-control" id="search"
                        placeholder="Search" name="search" value={searchInput} onChange={handleChange} />
                    <button class="btn btn-default"  onClick={handleClick}><FaIcons.FaSearchLocation id="icon" /></button>

                </div>
            
        </div>
        <Result repos={repos}/>
        </>
    )
}

export default SearchBar