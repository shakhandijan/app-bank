import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './Result.css'

const Result = (props) => {

    const { repos } = props

    const listRepos = repos.length !==0 ?  repos.data.map(
        item =>
            <tr key={item.id}>
                <td><a href={item.html_url}>{item.name}</a></td>
            </tr>
    ).slice(0, 5): <tr>No Projects</tr>

    return (

        <table className="table table-striped table-bordered">
            <thead className="table-dark">
                <tr>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
               {listRepos}  
            </tbody>
        </table>

    )
}

export default Result
