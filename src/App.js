import React from 'react'
import './App.css';
import SearchBar from './components/SearchBar';
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {


  return (
    <div className="App">
      <h1>Projects</h1>
      <SearchBar/>
    </div>
  );
}

export default App;
